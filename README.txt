FEEDS AUTH OPENAM
=================

A Feeds fetcher module that handles OpenAM authentication
https://www.drupal.org/sandbox/hedrickbt/2728667

Features
========

- Access Feed URIs that are secured by OpenAM
-- OpenAM Login Details
--- URL, Username, Password ( using the Key module for encryption )
--- Advanced
---- Feed request cookie name, Username header, Password header, Json response
       session Id, User-Agent
-- OpenAM Logout Details
--- URL
--- Advanced
---- Session Id Header, 
---- Successful Result search text


Requirements
============

- Key
  https://www.drupal.org/project/key
- Feeds
  https://www.drupal.org/project/feeds
-- Patch Required
  https://www.drupal.org/node/2728621
- Drupal 8.x
  http://drupal.org/project/drupal


Installation
============

- Install Key, Feeds OpenAM Authentication.
- Under Structure | Feed Types | Add feed type, select "Download Auth OpenAM"
  as the Fetcher on the Edit tab
- Under Content | Feeds | Add feed, choose the Feed Type you just created
  (only if you have more than 1 Feed Type), and then under the OpenAM section
  fill out the fields as your environment requires.  Sensible defaults are
  provided.  You will likely need to specify the Login URL, Username, Password,
  and Logout URL.
- Import the Feed as you would normally for a "Download" Feed.
