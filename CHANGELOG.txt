Feeds OpenAM Authentication 8.x-1.x-dev
---------------------------------------

7.8.2016
- By hedrickbt: Many, many pareview warning/formatting issues resolved
- By hedrickbt: Removed the redundant EventSubscriber\OpenAmLazySubscriber
    that was causing duplicate content items to be created since the
    feed was being loaded twice.  This also meant I no longer needed
    the feeds_auth_openam_services.yml file.

5.20.2016
- By hedrickbt: Changed root request from ^ to server: to mimic base:
- By hedrickbt: Added detail to the description for the URL fields.
- By hedrickbt
 - Added support for URIs that start with ^ ( indicate the root of the server )
 - Added support for URIs that start with base
 - Changed the source field (Feed URL) to a textfield from URL to support
     standard URI patterns, ^, and base
- By hedrickbt: Added support for URIs that start with /
- By hedrickbt: Initial check in
